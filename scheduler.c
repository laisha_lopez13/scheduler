#include "scheduler.h"

    task *crear_Task(int _id, int _delay,char *_nombreTask, char *_proceso_realizar, int _estado){
        task *_t = (task *)malloc(sizeof(task));
        _t->id = _id;
        _t->delay = _delay;
        strcpy(_t->nombreTask, _nombreTask);
        strcpy(_t->proceso_realizar, _proceso_realizar);
        _t->estado = _estado;
        return _t;
    }

    void agregar_Task(task *_array, task _t,int _index){
        _array[_index] = _t;
    }

    void ejecutar_Tasks(task *_array){
        for(int i = 0; i < MAX_TASK; i++){
            if(_array[i].estado == ACTIVO){
                printf("<----------------------------------------------------------------->\n");
                printf("\t La tarea de NOMBRE [%s], con ID [%d], se encuentra realizando el proceso [%s] con un delay de [%d]\n", _array[i].nombreTask, _array[i].id, _array[i].proceso_realizar, _array[i].delay);
                printf("<----------------------------------------------------------------->\n");
                for(int j=0; j<_array[i].delay; j++){
                    sleep(1);
                }
            }
        }
    }