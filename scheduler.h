#ifndef __SCHEDULER_H
#define __SCHEDULER_H

#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define MAX_STRING

#define MAX_TASK


#define UN_SEGUNDO

#define DOS_SEGUNDOS

#define TRES_SEGUNDOS


#define ACTIVO

#define NO_ACTIVO


typedef struct TASK task;
struct TASK{
    int id;
    int delay;
    char nombreTask[MAX_STRING];
    char proceso_realizar[MAX_STRING];
    int estado;
};
task *crear_Task(int _id, int _delay, char *_nombreTask, char *_proceso_realizar, int _estado);
void agregar_Task(task *_array, task _t, int _index);
void ejecutar_Tasks(task *_array);

#endif