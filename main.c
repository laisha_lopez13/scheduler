#include "scheduler.h"

int main (int argc, char const *argv[]){
    task *t1 = crear_Task(1, UN_SEGUNDO,"Tarea_1","Leer", ACTIVO);
    task *t2 = crear_Task(2, DOS_SEGUNDOS,"Tarea_2","Abrir", NO_ACTIVO);
    task *t3 = crear_Task(3, TRES_SEGUNDOS,"Tarea_3","Escribir", ACTIVO);
    task *t4 = crear_Task(4, TRES_SEGUNDOS,"Tarea_4","Eliminar", ACTIVO);

    task *array = ( task *)malloc(sizeof(task)*MAX_TASK);

    agregar_Task(array, *t1, 0);
    agregar_Task(array, *t2, 1);
    agregar_Task(array, *t3, 2);
    agregar_Task(array, *t4, 3);

    ejecutar_Tasks(array);

    free(t1);
    free(t2);
    free(t3);
    free(t4);

    return 0;
}